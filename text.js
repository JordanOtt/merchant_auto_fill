  var math = function(number) {
    return Math.floor(Math.random() * number).toString();
  }
  getName = function(firstLast) {
    var firstNames = ["Steve", "Kevin", "Stacy", "Jessica", "Liam", "Charles", "Lenny", "Clarissa", "Jenny", "Lexington", "Patty", "Brumhilda",
  "Hanibal", "Jermain", "Hayley", "Gerard", "DeAngelo", "William", "Keeree", "Chester"];
    var lastNames = ["Williams", "Stefanopolous", "Bird", "Stevenson", "Lennon", "Gerardi", "Hellman", "Johnson", "Otto", "Banbury", "Mccinnes",
  "Woods", "Shapiro", "Capote", "Harvey", "Allred", "Rodriguez", "Smith", "Ortiz", "Lidell"];

    if (firstLast == 1) {
      return firstNames[math(firstNames.length)];
    }else {
      return lastNames[math(lastNames.length)];
    }
  }

  function Person(first, last) {
    this.firstName = first;
    this.lastName = last;
  }

  getEmail = function(firstName, lastName) {
    person = new Person(getName(1), getName(2))
    return firstName + "." + lastName + "@gmail.com";
  }

  getPosition = function() {
    var positions = ["Business Owner", "Store Manager", "Store Sales Staff", "Store Finance Staff"];
    return positions[math(positions.length)];
  }

  getTaxId = function(number) {
    if (number.length === 9) {
      return number;
    }else {
      number = math(1000000000);
      getTaxId(number);
    }
  }

  fillOutMerchantInfo = function() {
    var prefix = "#merchant"
    var lbn = "The Store" + " " + math(255)
    $(prefix + "_legal_business_name").val(lbn).blur();
    $(prefix + "_dba").val(lbn).blur();
    $(prefix + "_tax_id_number").val(getTaxId(math(1000000000))).blur();
    $(prefix + "_year_established").val(parseInt(math(100)) + 1900).blur();
    $(prefix + "_number_of_locations").val(parseInt(math(3)) + 1).blur();
    $(prefix + "_website_url").val("thestore.com").blur();
    $(prefix + "_hear_about_us").val('Internet search').blur();
    $(prefix + "_hear_about_us_comments").val('The internet').blur();
    $(prefix + "_source").val('Direct Sale').blur();

  }

  fillOutLocationInfo = function() {
    getAddress = function() {
      var address = [
                {
                  street: '2760 S Highland Dr',
                  city: 'Salt Lake City',
                  zip: '84106'
                },
                {
                  street: '1054 S Concord St',
                  city: 'Salt Lake City',
                  zip: '84104'
                },
                {
                  street: '384 Logan Ave',
                  city: 'Salt Lake City',
                  zip: '84115'
                },
                {
                  street: '1432 E Capella Way',
                  city: 'Sandy',
                  zip: '84093'
                },
                {
                  street: '6362 S Clay Park Dr',
                  city: 'Murray',
                  zip: '84107'
                }
              ];
      return address[math(address.length)];
    }
    var prefix = "#merchant_locations_attributes_0";
    var getLocationAddress = getAddress();
    $(prefix + "_dba").val("location" + " " + math(3) + 1).blur();
    $(prefix + "_square_feet").val("3000").blur();
    $(prefix + "_primary_contact_name").val(getName(1) + " " + getName(2)).blur();
    $(prefix + "_primary_contact_title").val(getPosition()).blur();
    $(prefix + "_address_1").val(getLocationAddress.street).blur();
    $(prefix + "_secondary_contact_name").val(getName(1) + " " + getName(2)).blur();
    $(prefix + "_secondary_contact_title").val(getPosition()).blur();
    $(prefix + "_city").val(getLocationAddress.city).blur();
    $(prefix + "_state_code").val("UT").blur();
    $(prefix + "_zip").val(getLocationAddress.zip).blur();
    $(prefix + "_phone").val("8015551000").blur();
    $(prefix + "_sales_rep_id").val("394").blur();
  }

  fillOutLeaseContactInfo = function() {
    var prefix = "#merchant_locations_attributes_0"
    $(prefix + "_email").val(getEmail(getName(1), getName(2))).blur();
    $(prefix + "_funding_email").val(getEmail(getName(1), getName(2))).blur();
    $(prefix + "_fax").val("8005551000").blur();
  }

  fillOutOwnerInfo = function() {
    var owner = "#merchant_owners";
    var merchantOwner = new Person(getName(1), getName(2))
    $(owner + "_first_name").val(merchantOwner.firstName).blur();
    $(owner + "_last_name").val(merchantOwner.lastName).blur();
    $(owner + "_ssn").val("123456789").blur();
    $(owner + "_email").val(getEmail(merchantOwner.firstName, merchantOwner.lastName)).blur();
    $(owner + "_phone").val("8015551000").blur();
  }

  fillOutUserInfo = function() {
    var merchantUser = new Person(getName(1), getName(2))
    var user = "#merchant_merchant_users"
    $(user + "_first_name").val(merchantUser.firstName).blur();
    $(user + "_last_name").val(merchantUser.lastName).blur();
    $(user + "_title").val(getPosition()).blur();
    $(user + "_phone").val("8015551001").blur();
    $(user + "_username").val(merchantUser.firstName + "." + merchantUser.lastName).blur();
    $(user + "_email").val(getEmail(merchantUser.firstName, merchantUser.lastName)).blur();
  }

  fillOutBankInfo = function() {
    var prefix = "#merchant_bank_accounts_attributes_0";
    $(prefix + "_routing_number").val("124002971").blur();
    $(prefix + "_account_number").val(math(1000000000)).blur();
  }

  fillOutMerchantInfo();
  fillOutLocationInfo();
  fillOutLeaseContactInfo();
  fillOutOwnerInfo();
  fillOutUserInfo();
  fillOutBankInfo();
